import sys
from OptPblm import *
import json
file1 = "/home/marius/Documents/trier/dhn/dhn-expansion/dhn-expansion-shared/code/src/network_data/Aroma_tree_system.json"
file2 = "/home/marius/Documents/trier/dhn/dhn-expansion/dhn-expansion-shared/code/src/network_data/Aroma_tree_scenario.json"
file3 = "/home/marius/Documents/trier/dhn/dhn-expansion/dhn-expansion-shared/code/src/parameters/parameters.json"
file4 = "/home/marius/Documents/trier/dhn/dhn-expansion/dhn-expansion-shared/code/src/parameters/computational_parameters.json"
file5 = "/home/marius/Documents/trier/dhn/dhn-expansion/dhn-expansion-shared/code/src/demand_data/demand_profiles.json"
file6 = "/home/marius/Documents/trier/dhn/dhn-expansion/dhn-expansion-shared/code/src/network_data/Aroma_tree_coefficients.json"
file7 = "/home/marius/Documents/trier/dhn/dhn-expansion/dhn-expansion-shared/code/src/parameters/pipe_prices.json"
file8 = "/home/marius/Documents/trier/dhn/dhn-expansion/dhn-expansion-shared/code/src/parameters/peak_prices.json"

def import_json(file):
    with open(file) as json_file:
        return json.load(json_file)

data1 = import_json(file1)
data2 = import_json(file2)
data3 = import_json(file3)
data4 = import_json(file4)
data5 = import_json(file5)
data7 = import_json(file7)
data8 = import_json(file8)

DHN1 = DHN(data1, data2, data7, data8)
DHN1.flow_direction_presolve()
rho = data3['rho']
cp = data3['specific_heat_coefficient']
Tsoil = 278
degree = data4["degree_of_approx"]
solver_of_approx = data4["solver_of_approx"]
#DHN1.tikz_node_position(24)
#DHN1.tikz_edge()
DHN1.fitting_computation(cp, rho, Tsoil, degree, solver_of_approx, file6, not_computed=False)

# DHN1.plot_network(with_arcs = True, with_colors = True, with_distinction = True)
# for e in DHN1.edges:
#     if (not (DHN1.edges[e]['flow_is_fixed_pos'] or DHN1.edges[e]['flow_is_fixed_neg'])):
#         print(e)

Pblm = OptPblm()
Pblm.load_data(DHN1,data5,data3,data4)
Pblm.bigM_path_computation()
Pblm.setup_computation()
Pblm.solve_instance()
#Pblm.save_results("/home/marius/Documents/Trier/DHN/dhn-expansion/code/src/data_extraction/node_sol.tikz","/home/marius/Documents/Trier/DHN/dhn-expansion/code/src/data_extraction/edge_sol.tikz","/home/marius/Documents/Trier/DHN/dhn-expansion/paper/figures/foreflow/06U.tikz")
Pblm.plot_results()
