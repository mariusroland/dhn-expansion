from networkx import *
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from math import sqrt,log,exp,pi
from pyomo.environ import *
from pyomo.opt import SolverFactory
import json
import csv


#We set up a class for DHN instances
class DHN(DiGraph):
    #Constructor that creates a networkx graph and some object variables
    def __init__(self, network_dict, consumer_info_dict, pipe_prices, peak_costs):
        print("Started DHN initialization")
        ###Define relevant functions and adapt some values in order to get the right units###
        #Define function that removes 'id' key in dictionnary
        #and returns that new dictionary
        def node_without_id(d):
            new_d = d.copy()
            new_d.pop('id')
            new_d["existing"] = (d["existing"] == 1)
            return new_d
        #We do the same for the 'id', 'start_id' and 'end_id' and we add the slope and length
        #to the dictionnary
        def arc_without_id(d,given_nodes,is_consumer=False,is_source=False):
            new_d = d.copy()
            new_d.pop('start_node_id')
            new_d.pop('end_node_id')
            new_d['is_consumer'] = is_consumer
            if not (is_consumer or is_source):
                for n in given_nodes:
                    if n["id"] == d['start_node_id']:
                        start_x = n['position'][0]
                        start_y = n['position'][1]
                        start_z = n['position'][2]
                    elif n["id"] == d['end_node_id']:
                        end_x = n['position'][0]
                        end_y = n['position'][1]
                        end_z = n['position'][2]
                new_d['length'] = sqrt(pow((end_x-start_x),2)+pow((end_y-start_y),2)+pow((end_z-start_z),2))
                new_d['slope'] = (end_z-start_z)/new_d['length']
                new_d['pipe_price'] = pipe_prices[str(new_d['diameter'])]
            new_d['is_source'] = is_source
            new_d['is_pipe'] = not (is_source or is_consumer)
            new_d["existing"] = (d["existing"] == 1)
            found = False
            if is_consumer and (not new_d['existing']):
                for k in peak_costs.keys():
                    if float(k) >= abs(new_d['consumer_factor']/24*3) and (not found):
                        new_d['peak_cost'] = peak_costs[k]/365
                        found = True
            return new_d
        def merge_dicts(dict1, dict2):
            return(dict2.update(dict1))
        #Merge info of the relevant dictionnaries for the consumers
        for c1 in network_dict['consumers']:
            for c2 in consumer_info_dict:
                if c1['id'] == c2['id']:
                    merge_dicts(c2,c1)
        #Create nodes, pipes and consumers for the graph definition
        #and create the usefull class variables
        for p in network_dict['pipes']:
            p['diameter'] = p['diameter']/1e3
            p['roughness'] = p['roughness']/1e3
        
        ###Create the graph with the relevant attributes###
        nodes = [(n['id'],node_without_id(n)) for n in network_dict['nodes']]
        nodes_ids = [n['id'] for n in network_dict['nodes']]
        pipes = [(p['start_node_id'],p['end_node_id'],arc_without_id(p,network_dict['nodes'])) for p in network_dict['pipes']]
        pipes_ids = [(p['start_node_id'],p['end_node_id']) for p in network_dict['pipes']]
        consumers = [(c['start_node_id'],c['end_node_id'],arc_without_id(c,network_dict['nodes'],is_consumer=True)) for c in network_dict['consumers']]
        consumers_ids = [(c['start_node_id'],c['end_node_id']) for c in network_dict['consumers']]
        sources = [(s['start_node_id'],s['end_node_id'],arc_without_id(s,network_dict['nodes'],is_source=True)) for s in network_dict['sources']]
        sources_ids = [(s['start_node_id'],s['end_node_id']) for s in network_dict['sources']]
        super().__init__()
        super().add_nodes_from(nodes)
        super().add_edges_from(pipes)
        super().add_edges_from(consumers)
        super().add_edges_from(sources)
        self.pipes_ids = pipes_ids
        self.consumers_ids = consumers_ids
        self.sources_ids = sources_ids

        ###Foreflow and backflow detection###
        #Initialize
        for n in self.nodes:
            self.nodes[n]["in_backflow"] = False
            self.nodes[n]["in_foreflow"] = False
        temp_G = Graph()
        temp_G.add_nodes_from(nodes_ids)
        temp_G.add_edges_from(pipes_ids)
        visited_nodes_backflow = [s[0] for s in sources_ids]
        visited_nodes_foreflow = [s[1] for s in sources_ids]
        for s in sources_ids:
            self.nodes[s[0]]["in_backflow"] = True
            self.nodes[s[1]]["in_foreflow"] = True

        #Apply depth first search
        neighbors_available = True
        while neighbors_available:
            neighbors_available = False
            for n1 in visited_nodes_backflow:
                for n2 in temp_G.neighbors(n1):
                    if n2 not in visited_nodes_backflow:
                        visited_nodes_backflow.append(n2)
                        self.nodes[n2]["in_backflow"] = True
                        neighbors_available = True

        neighbors_available = True
        while neighbors_available:
            neighbors_available = False
            for n1 in visited_nodes_foreflow:
                for n2 in temp_G.neighbors(n1):
                    if n2 not in visited_nodes_foreflow:
                        visited_nodes_foreflow.append(n2)
                        self.nodes[n2]["in_foreflow"] = True
                        neighbors_available = True
        print("Finished network initialization")



    #We define getters in order to esily acces the network info
    def get_pipes(self):
        return [(e[0],e[1],self.edges[e]) for e in self.edges if self.edges[e]["is_source"] != True and self.edges[e]["is_consumer"] != True]

    def get_consumers(self):
        return [(e[0],e[1],self.edges[e]) for e in self.edges if self.edges[e]["is_consumer"]]

    def get_sources(self):
        return [(e[0],e[1],self.edges[e]) for e in self.edges if self.edges[e]["is_source"]]

    def get_slopes(self):
        for e in self.edges:
            print((e[0],e[1],self.edges[e]))

    def get_pipes_ids(self):
        return self.pipes_ids

    def get_consumers_ids(self):
        return self.consumers_ids

    def get_sources_ids(self):
        return self.sources_ids

    def get_len_pipes(self):
        return len(self.pipes_ids)

    def get_len_consumers(self):
        return len(self.consumers_ids)

    def get_len_sources(self):
        return len(self.sources_ids)

    @staticmethod
    def table_to_csv(table, file_name, nl, dl):
        with open(file_name, 'w', newline=nl) as csvfile:
            csvwriter = csv.writer(csvfile, delimiter=dl,
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
            for l in table:
                csvwriter.writerow(l)
                
    def tikz_node_position(self, page_size):
        scale = 2400/page_size
        node_list = [[str(self.nodes[n]["position"][0]/scale), str(self.nodes[n]["position"][1]/scale), n+ ","] for n in self.nodes if self.nodes[n]["in_foreflow"]]
        node_list[len(node_list)-1][len(node_list[len(node_list)-1])-1] = node_list[len(node_list)-1][len(node_list[len(node_list)-1])-1][0:-1]
        file_name = "/home/marius/Documents/trier/dhn/dhn-expansion/dhn-expansion-shared/paper/figures/node_pos.tikz"
        self.table_to_csv(node_list, file_name, '\n', '/')
    
    def tikz_edge(self):
        for e in self.edges:
            if self.edges[e]["is_pipe"]:
                print('('+e[0]+','+e[1]+')'+' & '+ str(self.edges[e]["diameter"]*1000)+'\\\\')

    def fitting_computation(self, cp, rho, Tsoil, degree, solver_of_approx, json_file, not_computed = True):
        print("Started fitting computation")
        scaling_var = 1e-10
        if not_computed:
            #Creation of abstract model
            m = AbstractModel()
            m.I = Set()
            m.J = Set()
            m.KLN = Set(dimen=3)
            m.Tsoil = Param()
            m.v = Param(m.I)
            m.T0 = Param(m.J)
            m.TLa = Param(m.I, m.J)
            m.scaling = Param()            
            m.alpha = Var(m.KLN)
            def Obj_rule(m):
                return (sum(sum((m.v[i]**k)*(m.T0[j]**l)*(m.TLa[i,j]**n)*(m.v[i]**o)*(m.T0[j]**p)*(m.TLa[i,j]**q) for i in m.I for j in m.J)*m.scaling*m.alpha[k,l,n]*m.alpha[o,p,q] for (k,l,n) in m.KLN for (o,p,q) in m.KLN)
                           + sum(m.alpha[k,l,n]*sum(2*(m.TLa[i,j] - m.Tsoil)*(m.v[i]**k)*(m.T0[j]**l)*(m.TLa[i,j]**n) for i in m.I for j in m.J)*m.scaling for (k,l,n) in m.KLN)
                           + sum(((m.TLa[i,j] - m.Tsoil)**2)*m.scaling for i in m.I for j in m.J))
            m.Obj = Objective(rule = Obj_rule)
            #Creation and solving of instance
            T0_max = 403
            size_I = 3200
            size_J = 40
            T0_delta = (T0_max-Tsoil)/(size_J-1)
            T0_iter = [Tsoil + j*T0_delta for j in range(size_J)]
            data = []
            d = degree
            def T_real(U,L,cp,rho,D,Tsoil,v,T0):
                if v == 0:
                    return Tsoil
                else:
                    return T0*exp(-4*U*L/(cp*rho*D*v))+Tsoil*(1-exp(-4*U*L/(cp*rho*D*v)))
            for e in self.edges:
                if self.edges[e]["is_pipe"]:
                    D = self.edges[e]['diameter']
                    r = D/2
                    A = pi*r*r
                    L = self.edges[e]['length']
                    U = self.edges[e]['ht_coefficient']
                    vmax = self.edges[e]['q_plus']/A/rho
                    v_delta = vmax/(size_I-1)
                    v_iter = [i*v_delta for i in range(size_I)]
                    data_dict = {}
                    data_dict["I"] = {None:list(range(size_I))}
                    data_dict["J"] = {None:list(range(size_J))}
                    data_dict["KLN"] = {None: [(k,l,n) for k in range(1,d+1) for l in range(d+1) for n in range(d+1) if l+k+n <= d]}
                    data_dict["Tsoil"] = {None: Tsoil}
                    data_dict["v"] = {i: v_iter[i] for i in range(size_I)}
                    data_dict["T0"] = {j: T0_iter[j] for j in range(size_J)}
                    data_dict["TLa"] = {(i,j): T_real(U,L,cp,rho,D,Tsoil,v_iter[i],T0_iter[j]) for i in range(size_I) for j in range(size_J)}
                    data_dict['scaling'] = {None: scaling_var}
                    final = {None: data_dict}
                    i = m.create_instance(final)
                    #i.pprint()
                    opt = SolverFactory('gams')
                    results = opt.solve(i, tee = False, solver = solver_of_approx, mtype = 'qcp', add_options = ['option optcr=0.01;','option optca=100;'])
                    temp_dict = {str((k,l,n)): i.alpha[k,l,n].value for (k,l,n) in i.KLN}
                    print(value(i.Obj)/scaling_var)
                    self.edges[e]['alpha'] = temp_dict
                    data.append({'id':e,'alpha':temp_dict})
            with open(json_file, 'w') as outfile:
                json.dump(data, outfile)
        else:
            with open(json_file) as infile:
                data = json.load(infile)
            for e in data:
                self.edges[e['id']]['alpha'] = e['alpha'] 
        print("Finished fitting computation")
    
    #Method that applies the flow presolve algorithm#
    def flow_direction_presolve(self):
        print("Started flow direction presolve")
        #Initialize necessary stuff
        set_A_pos = self.get_sources_ids() + self.get_consumers_ids()
        def diff(first, second):
            second = set(second)
            return [item for item in first if item not in second]
        G_hat = Graph()
        G_hat.add_edges_from(diff(self.edges,set_A_pos))
        def merge_dicts(dict1, dict2):
            return(dict2.update(dict1))
        link_new_G_hat_edges_to_G_edges = {(e[1],e[0]):e for e in self.edges}
        merge_dicts({e:e for e in self.edges},link_new_G_hat_edges_to_G_edges)
        #Compute 2-connected components
        components = sorted(map(sorted, k_edge_components(G_hat,k=2)))
        #Create dicts that link the nodes from Ghat to the new nodes of Ghat
        link_G_to_new_G_hat = {components[c][n]:c+1 for c in range(0,len(components)) for n in range(len(components[c]))}
        #Create new edges
        new_edges = []
        link_new_G_hat_edges_to_G_edges_pos = {}
        link_new_G_hat_edges_to_G_edges_neg = {}
        for e in diff(self.edges,set_A_pos):
            link_e0 = link_G_to_new_G_hat[e[0]]
            link_e1 = link_G_to_new_G_hat[e[1]]
            if link_e0 != link_e1:
                new_edges.append((link_e0,link_e1))
                link_new_G_hat_edges_to_G_edges_pos[(link_e0,link_e1)] = e
                link_new_G_hat_edges_to_G_edges_neg[(link_e1,link_e0)] = e
        #Create new Ghat
        new_G_hat = Graph()
        new_G_hat.add_edges_from(new_edges)
        visited_nodes_foreflow = [link_G_to_new_G_hat[s[1]] for s in self.get_sources_ids()]
        visited_edges_foreflow = []
        still_nodes_to_add = True
        while still_nodes_to_add:
            still_nodes_to_add = False
            for n1 in visited_nodes_foreflow:
                for n2 in new_G_hat.neighbors(n1):
                    if n2 not in visited_nodes_foreflow:
                        visited_nodes_foreflow.append(n2)
                        visited_edges_foreflow.append((n1,n2))
                        still_nodes_to_add = True
        visited_nodes_backflow = [link_G_to_new_G_hat[s[0]] for s in self.get_sources_ids()]
        visited_edges_backflow = []
        still_nodes_to_add = True
        while still_nodes_to_add:
            still_nodes_to_add = False
            for n1 in visited_nodes_backflow:
                for n2 in new_G_hat.neighbors(n1):
                    if n2 not in visited_nodes_backflow:
                        visited_nodes_backflow.append(n2)
                        visited_edges_backflow.append((n1,n2))
                        still_nodes_to_add = True

        for e in self.edges:
            self.edges[e]["flow_is_fixed_pos"] = False
            self.edges[e]["flow_is_fixed_neg"] = False

        for s in set_A_pos:
            self.edges[s]["flow_is_fixed_pos"] = True

        for new_G_hat_e in visited_edges_foreflow:
            if new_G_hat_e in link_new_G_hat_edges_to_G_edges_pos.keys():
                self.edges[link_new_G_hat_edges_to_G_edges_pos[new_G_hat_e]]["flow_is_fixed_pos"] = True
            elif new_G_hat_e in link_new_G_hat_edges_to_G_edges_neg.keys():
                self.edges[link_new_G_hat_edges_to_G_edges_neg[new_G_hat_e]]["flow_is_fixed_neg"] = True
        for new_G_hat_e in visited_edges_backflow:
            if new_G_hat_e in link_new_G_hat_edges_to_G_edges_pos.keys():
                self.edges[link_new_G_hat_edges_to_G_edges_pos[new_G_hat_e]]["flow_is_fixed_neg"] = True
            elif new_G_hat_e in link_new_G_hat_edges_to_G_edges_neg.keys():
                self.edges[link_new_G_hat_edges_to_G_edges_neg[new_G_hat_e]]["flow_is_fixed_pos"] = True

        for e in self.edges:
            self.edges[e]["flow_is_free"] =  not (self.edges[e]["flow_is_fixed_pos"] or self.edges[e]["flow_is_fixed_neg"])
        print("Finished flow direction presolve")
        
    #Method that plots the district heating network
    def plot_network(self, with_arcs = False, with_colors = False, with_distinction = False, display = False):
        no_height_given = True
        extra_height = 0
        # for n in self.nodes:
        #     if self.nodes[n]["position"][2] != 0:
        #         no_height_given = False
        max_distance = 0
        text_size = 6
        node_size = 200
        x = [self.nodes[n]["position"][0] for n in self.nodes]
        y = [self.nodes[n]["position"][1] for n in self.nodes]
        max_x = max(x)
        max_y = max(y)
        min_x = min(x)
        min_y = min(y)
        max_diff = max([max_x-min_x,max_y-min_y])
        factor = 1/500
        z = [self.nodes[n]["position"][2]-max_diff*factor*no_height_given*self.nodes[n]["in_backflow"] for n in self.nodes]
        x_for_lines = [[self.nodes[n1]["position"][0],self.nodes[n2]["position"][0]] for n1,n2 in self.edges]
        y_for_lines = [[self.nodes[n1]["position"][0],self.nodes[n2]["position"][0]] for n1,n2 in self.edges]
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        for n in self.nodes:
            x_pos = self.nodes[n]["position"][0]
            y_pos = self.nodes[n]["position"][1]
            z_pos = self.nodes[n]["position"][2]-max_diff*factor*no_height_given*self.nodes[n]["in_backflow"]
            ax.text(x_pos,y_pos,z_pos,n, color='white', ha='center', va='center', size=text_size, zorder=10)
        ax.scatter(x, y, z, s=node_size, marker='o', color='black', alpha = 1)
        if with_arcs:
            for n1,n2 in self.edges:
                x_for_lines = [self.nodes[n1]["position"][0],self.nodes[n2]["position"][0]]
                y_for_lines = [self.nodes[n1]["position"][1],self.nodes[n2]["position"][1]]
                z_for_lines = [self.nodes[n1]["position"][2]-max_diff*factor*no_height_given*self.nodes[n1]["in_backflow"],self.nodes[n2]["position"][2]-max_diff*factor*no_height_given*self.nodes[n2]["in_backflow"]]
                width_of_line = 3
                color_of_line = (('red'*self.edges[(n1,n2)]['is_source']+'green'*self.edges[(n1,n2)]['is_consumer']+'blue'*self.edges[(n1,n2)]['is_pipe'])*with_colors+'blue'*(not with_colors))
                style_of_line = ('-'*self.edges[(n1,n2)]['existing'] + '--'*(not self.edges[(n1,n2)]['existing']))*with_distinction + ('-')*(not with_distinction)
                ax.plot(x_for_lines,y_for_lines,z_for_lines, color = color_of_line, linewidth = width_of_line, linestyle = style_of_line)
        ax.grid(False)
        ax.axis('off')
        if display:
            fig.show()
            input()
        return node_size,no_height_given,max_diff,factor,text_size,ax,fig
