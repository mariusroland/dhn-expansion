from pyomo.environ import *
from DHN import *
import math
from pyomo.opt import SolverFactory
import json
class OptPblm:
    def __init__(self):
        print("Starting AbstractModel setup")
        ####OPTIMIZATION MODEL####
        
        ###Initialize the abstract model###
        self.m = AbstractModel()
        
        ###Initialize the data###
        
        ##Sets of the problem##
        #Edges#
        self.m.Apc = Set(dimen = 2)
        self.m.Ape = Set(dimen = 2)
        self.m.Ap = self.m.Apc | self.m.Ape
        self.m.Acc = Set(dimen = 2)
        self.m.Ace = Set(dimen = 2)
        self.m.Ac = self.m.Acc | self.m.Apc
        self.m.Acons = self.m.Acc | self.m.Ace
        self.m.Ad = Set(dimen = 2)
        self.m.Ae = self.m.Ape | self.m.Ace | self.m.Ad
        self.m.A = self.m.Ap | self.m.Acons | self.m.Ad
        #Nodes#
        self.m.Vc = Set()
        self.m.Ve = Set()
        self.m.Vff = Set()
        self.m.Vbf = Set()
        self.m.V = self.m.Vc | self.m.Ve
        #Spatial position#
        self.m.X = Set()
        #Orientation related
        self.m.delta_out = Set(dimen = 3)
        self.m.delta_in = Set(dimen = 3)
        self.m.pos_vel = Set(dimen = 2)
        self.m.neg_vel = Set(dimen = 2)
        self.m.O = Set(dimen = 3) #Indexed set using tuples (u,a) if flow in a is outgoing
        self.m.I = Set(dimen = 3) #Indexed set using tuples (u,a) if flow in a is ingoing
        #Path related
        self.m.Pa = Set(dimen = 4)
        #Approx related
        self.m.KLN = Set(dimen = 3)

        ##Sets for when we define multiple constraints at once##
        self.m.two_constraints = Set(initialize = [1,2])
        self.m.three_constraints = Set(initialize = [1,2,3])
        self.m.four_constraints = Set(initialize = [1,2,3,4])
        self.m.five_constraints = Set(initialize = [1,2,3,4,5])
        self.m.six_constraints = Set(initialize = [1,2,3,4,5,6])
        self.m.seven_constraints = Set(initialize = [1,2,3,4,5,7])
        

        ###Initialize the parameters###
        
        ##Parameters of the problem##
        self.m.upper_bound_on_Pw = Param()
        self.m.upper_bound_on_Pg = Param()
        self.m.upper_bound_on_Pp = Param()
        self.m.L = Param(self.m.Ap)
        self.m.D = Param(self.m.Ap)
        self.m.Area = Param(self.m.Ap)
        self.m.h = Param(self.m.Ap)
        self.m.Pc = Param(self.m.Acons)
        self.m.U = Param(self.m.Ap)
        self.m.alpha = Param(self.m.Ap, self.m.KLN)
        self.m.Tff = Param(self.m.Acons)
        self.m.Tbf = Param(self.m.Acons)
        self.m.Tsoil = Param()
        self.m.cp = Param()
        self.m.ps = Param()
        self.m.g = Param()
        self.m.Cinv = Param(self.m.Ac)
        self.m.w1 = Param()
        self.m.w2 = Param(self.m.Acc)
        self.m.Cw = Param()
        self.m.Cg = Param()
        self.m.Cp = Param()
        self.m.lamb = Param(self.m.Ap)
        self.m.q_plus = Param(self.m.A)
        self.m.p_plus = Param(self.m.V)
        self.m.p_minus = Param(self.m.V)
        self.m.Temp_plus = Param(self.m.V)
        self.m.Temp_minus = Param(self.m.V)
        self.m.Temp_edge_plus = Param(self.m.A)
        self.m.Temp_edge_minus = Param(self.m.A - self.m.Acons)
        self.m.Pw_plus = Param()
        self.m.Pg_plus = Param()
        self.m.Pp_plus = Param()
        self.m.rho = Param()
        self.m.seconds_in_day = Param()
        self.m.hours_in_day = Param()
        self.m.bigM1 = Param(self.m.Apc)
        self.m.bigM2 = Param(self.m.Apc)

        
        ###Initialize variables###
        
        ##Variables of the problem##
        self.m.q = Var(self.m.A)
        self.m.p_node = Var(self.m.V)
        self.m.Temp_node = Var(self.m.V)
        self.m.Temp_edge = Var(self.m.A, self.m.X)
        self.m.x = Var(self.m.Ac, domain=Binary)
        self.m.Pp = Var()
        self.m.Pw = Var()
        self.m.Pg = Var()
        

        ###Initialize Objective##
        def payment(m):
            return  (sum((m.Pc[a]*m.hours_in_day*m.w1 + m.w2[a])*m.x[a] for a in m.Acc) -
                    sum(m.Cinv[a]*m.x[a] for a in m.Ac) -
                    (m.hours_in_day*(m.Cw*m.Pw + m.Cg*m.Pg + m.Cp*m.Pp))
                    )
        self.m.obj = Objective(rule=payment, sense=maximize)

        ###Pipe constraints###
        
        ##Euler equation for icompressible fluids in existing pipes##
        def implicit_euler_existing_rule(m,u,v):
            a = (u,v)
            if (u,v) in m.pos_vel:
                return ((m.p_node[v]-m.p_node[u])/m.L[a] + m.lamb[a]*(m.q[a]*m.q[a]/(m.Area[a]*m.Area[a]*m.rho*2*m.D[a]))/1e5
                        + m.g*m.rho*m.h[a]/1e5 == 0)
            elif (u,v) in m.neg_vel:
                return ((m.p_node[v]-m.p_node[u])/m.L[a] - m.lamb[a]*(m.q[a]*m.q[a]/(m.Area[a]*m.Area[a]*m.rho*2*m.D[a]))/1e5
                        + m.g*m.rho*m.h[a]/1e5 == 0)
        self.m.euler_existing_eq = Constraint(self.m.Ape, rule=implicit_euler_existing_rule)

        ##Euler equation for icompressible fluids in candidate pipes##
        def implicit_euler_candidate_rule(m,u,v,mc):
            a = (u,v)
            if (u,v) in m.pos_vel:
                if mc == 1:
                    return ((m.p_node[v]-m.p_node[u])/m.L[a] + m.lamb[a]*(m.q[a]*m.q[a]/(m.Area[a]*m.Area[a]*m.rho*2*m.D[a]))/1e5
                        + m.g*m.rho*m.h[a]/1e5 <= (1 - m.x[a])*m.bigM1[a])
                elif mc == 2:
                    return ((m.p_node[v]-m.p_node[u])/m.L[a] + m.lamb[a]*(m.q[a]*m.q[a]/(m.Area[a]*m.Area[a]*m.rho*2*m.D[a]))/1e5
                        + m.g*m.rho*m.h[a]/1e5 >= (1 - m.x[a])*m.bigM2[a])
            elif (u,v) in m.neg_vel:
                if mc == 1:
                    return ((m.p_node[v]-m.p_node[u])/m.L[a] - m.lamb[a]*(m.q[a]*m.q[a]/(m.Area[a]*m.Area[a]*m.rho*2*m.D[a]))/1e5
                        + m.g*m.rho*m.h[a]/1e5 <= (1 - m.x[a])*m.bigM1[a])
                elif mc == 2:
                    return ((m.p_node[v]-m.p_node[u])/m.L[a] - m.lamb[a]*(m.q[a]*m.q[a]/(m.Area[a]*m.Area[a]*m.rho*2*m.D[a]))/1e5
                        + m.g*m.rho*m.h[a]/1e5 >= (1 - m.x[a])*m.bigM2[a])
        self.m.euler_candidate_eq = Constraint(self.m.Apc, self.m.two_constraints, rule=implicit_euler_candidate_rule)

        ##Thermal energy equation##
        def thermal_rule(m,u,v):
            a = (u,v)
            return sum(m.alpha[u,v,k,l,n]*((m.q[a]/m.Area[a]/m.rho)**k)*(m.Temp_edge[a,'0']**l)*(m.Temp_edge[a, 'La'])**n for (k,l,n) in m.KLN) + m.Temp_edge[a, 'La'] - m.Tsoil == 0
        self.m.thermal_eq = Constraint(self.m.Ap, rule=thermal_rule)

        ##Mass balance equation##
        def mass_balance_rule(m,u):
            return (sum(m.q[(a[1],a[2])] for a in m.delta_in if a[0] == u)
                    - sum(m.q[(a[1],a[2])] for a in m.delta_out if a[0] == u) == 0)
        self.m.mass_balance = Constraint(self.m.V, rule = mass_balance_rule)

        ##Temperature mixing equation nr1##
        def temperature_mixing1_rule(m,u):
            return (sum(-m.q[(e,d)]*m.Temp_edge[(e,d),'0'] for (v,e,d) in m.I if v == u and e == u)
                    + sum(m.q[(e,d)]*m.Temp_edge[(e,d),'La'] for (v,e,d) in m.I if v == u and d == u)
                    - m.Temp_node[u]*(sum(-m.q[(e,d)] for (v,e,d) in m.I if v == u and e == u)
                    + sum(m.q[(e,d)] for (v,e,d) in m.I if v == u and d == u)) == 0)
        self.m.temperature_mixing1 = Constraint(self.m.V, rule = temperature_mixing1_rule)

        ##Temperature mixing equation nr2##
        def temperature_mixing2_rule(m,u,d,e):
            if u == d:
                return m.Temp_node[u] - m.Temp_edge[(d,e),'0'] == 0
            elif u == e:
                return  m.Temp_node[u] - m.Temp_edge[(d,e),'La'] == 0
        self.m.temperature_mixing2 = Constraint(self.m.O, rule = temperature_mixing2_rule)
            
        ##Edge Dependent bounds for existing edges##
        def bounds_edge_dependent_existing_rule(m,u,v,mc):
            a = (u,v)
            if mc == 1:
                if (u,v) in m.pos_vel:
                    return m.q[a] >= 0
                elif (u,v) in m.neg_vel:
                    return -m.q[a] >= 0
            elif mc == 2:
                if (u,v) in m.pos_vel:
                    return m.q[a] <= m.q_plus[a]
                elif (u,v) in m.neg_vel:
                    return -m.q[a] <= m.q_plus[a]
            elif mc == 3:
                if a not in m.Acons:
                    return m.Temp_edge[a,'0'] >= m.Temp_edge_minus[a]
                else:
                    return Constraint.Skip
            elif mc == 4:
                if a not in m.Acons:
                    return m.Temp_edge[a,'La'] >= m.Temp_edge_minus[a]
                else:
                    return Constraint.Skip
            elif mc == 5:
                return m.Temp_edge[a,'0'] <= m.Temp_edge_plus[a]
            elif mc == 6:
                if a not in m.Acons:
                    return m.Temp_edge[a,'La'] <= m.Temp_edge_plus[a]
                else:
                    return Constraint.Skip
        self.m.bounds_edge_dependent_existing = Constraint(self.m.Ae,
                                                           self.m.six_constraints, rule = bounds_edge_dependent_existing_rule)

        ##Edge dependent bounds for canididate nodes##
        def bounds_edge_dependent_candidate_rule(m,u,v,mc):
            a = (u,v)
            if mc ==1:
                if (u,v) in m.pos_vel:
                    return m.q[a] >= 0
                elif (u,v) in m.neg_vel:
                    return -m.q[a] >= 0
            elif mc == 2:
                if (u,v) in m.pos_vel:
                    return m.q[a] <= m.q_plus[a]*m.x[a]
                elif (u,v) in m.neg_vel:
                    return -m.q[a] <= m.q_plus[a]*m.x[a]
            elif mc == 3:
                if a not in m.Acons:
                    return m.Temp_edge[a,'0'] >= m.Temp_edge_minus[a]
                else:
                    return Constraint.Skip
            elif mc == 4:
                if a not in m.Acons:
                    return m.Temp_edge[a,'La'] >= m.Temp_edge_minus[a]
                else:
                    return Constraint.Skip
            elif mc == 5:
                return m.Temp_edge[a,'0'] <= m.Temp_edge_plus[a]
            elif mc == 6:
                if a not in m.Acons:
                    return m.Temp_edge[a,'La'] <= m.Temp_edge_plus[a]
                else:
                    return Constraint.Skip
        self.m.bounds_edge_dependent_candidate = Constraint(self.m.Ac,
                                                           self.m.six_constraints, rule = bounds_edge_dependent_candidate_rule)
        
        ##Node dependent bounds for existing nodes##
        def bounds_node_dependent_rule(m,u,mc):
            if mc == 1:
                return m.p_minus[u] <= m.p_node[u]
            elif mc == 2:
                return m.p_node[u] <= m.p_plus[u]
            elif mc == 3:
                return m.Temp_minus[u] <= m.Temp_node[u]
            elif mc == 4:
                return m.Temp_node[u] <= m.Temp_plus[u]
        self.m.bounds_node_dependent = Constraint(self.m.V, self.m.four_constraints, rule = bounds_node_dependent_rule)

        
        ##Simple bounds node independent##
        def bounds_node_independent_rule(m,mc):
            if mc == 1:
                if m.upper_bound_on_Pp:
                    return  m.Pp <= value(m.Pp_plus)
                else:
                    return Constraint.Skip
            elif mc == 2:
                return m.Pp >= 0
            elif mc == 3:
                return 0 <= m.Pw
            elif mc == 4:
                if m.upper_bound_on_Pw:
                    return m.Pw <= value(m.Pw_plus)
                else:
                    return Constraint.Skip
            elif mc == 5:
                if m.upper_bound_on_Pg:
                    return  m.Pg <= value(m.Pg_plus)
                else:
                    return Constraint.Skip
            elif mc == 6:
                return m.Pg >= 0
        self.m.bounds_node_independent = Constraint(self.m.six_constraints, rule = bounds_node_independent_rule)

        ###Consumer constraints###

        ##Existing/Candidate consumer constraints##
        def consumer_constraints_rule(m,c,d,mc):
            a = (c,d)
            if mc == 1:
                if a in m.Acc:
                    return (m.Pc[a]*m.x[a] - m.cp*m.q[a]*(m.Temp_edge[(a,'0')] - m.Temp_edge[(a,'La')])/1e3 == 0)
                else:
                    return (m.Pc[a] - m.cp*m.q[a]*(m.Temp_edge[(a,'0')] - m.Temp_edge[(a,'La')])/1e3 == 0)
            elif mc == 2:
                return m.Tbf[a]  == m.Temp_edge[(a,'La')]
            elif mc == 3:
                return m.Temp_edge[(a,'0')] - m.Tff[a] >= 0
            elif mc == 4:
                return m.p_node[c] - m.p_node[d] >= 0
        self.m.consumer_constraints = Constraint(self.m.Acons, self.m.four_constraints, rule = consumer_constraints_rule)

        ###Depot Constraints###

        ##Depot Constraints##
        def depot_constraints_rule(m,c,d,mc):
            a = (c,d)
            if mc == 1:
                return m.ps  == m.p_node[c]
            elif mc == 2:
                return m.Pp/1e2 - m.q[a]/m.rho*(m.p_node[d]-m.p_node[c]) == 0
            elif mc == 3:
                 return m.Pw + m.Pg - m.cp*m.q[a]*(m.Temp_edge[(a,'La')] - m.Temp_edge[(a,'0')])/1e3 == 0
        self.m.depot_constraints = Constraint(self.m.Ad, self.m.three_constraints, rule = depot_constraints_rule)

        def path_constraint_rule(m,u1,v1,u2,v2):
            a = (u1,v1)
            b = (u2,v2)
            return m.x[a] <= m.x[b]
        self.m.path_constraint = Constraint(self.m.Pa, rule = path_constraint_rule)
        print("Finished with AbstractModel setup")

    def load_data(self,network,demand_profiles,parameters,computational_parameters):
        self.network = network
        self.demand_profiles = demand_profiles
        self.parameters = parameters
        self.computational_parameters = computational_parameters

    def bigM_path_computation(self):
        print("Starting big-M/path computation")
        depot = self.network.get_sources_ids()[0]
        visited = [depot[0],depot[1]]
        pipes = self.network.get_pipes_ids()
        paths = {}
        G = Graph()
        G.add_edges_from(pipes)
        while len(visited) != self.network.number_of_nodes():
            for n1 in visited:
                for n2 in G.neighbors(n1):
                    if n2 not in visited:
                        visited.append(n2)
                        if self.network.nodes[n1]['existing'] and (not self.network.nodes[n2]['existing']):
                            if (n1,n2) in self.network.edges.keys():
                                self.network.edges[(n1,n2)]['path'] = []
                                paths[n2] = [(n1,n2)]
                            elif (n2,n1) in self.network.edges.keys():
                                self.network.edges[(n2,n1)]['path'] = []
                                paths[n2] = [(n2,n1)]
                        elif (not self.network.nodes[n1]['existing']) and (not self.network.nodes[n2]['existing']):
                            if (n1,n2) in self.network.edges.keys():
                                self.network.edges[(n1,n2)]['path'] = paths[n1]
                                paths[n2] = paths[n1].copy()
                                paths[n2].append((n1,n2))
                            elif (n2,n1) in self.network.edges.keys():
                                self.network.edges[(n2,n1)]['path'] = paths[n1]
                                paths[n2] = paths[n1].copy()
                                paths[n2].append((n2,n1))
        for e in self.network.edges:
            if not self.network.edges[e]['existing'] and self.network.edges[e]['is_consumer']:
                self.network.edges[e]['path'] = paths[e[0]]+paths[e[1]]
        for a in self.network.edges:
            if (not self.network.edges[a]['existing']) and self.network.edges[a]['is_pipe']:
                self.network.edges[a]['bigM1'] = ((self.network.nodes[a[1]]['p_plus'] - self.network.nodes[a[0]]['p_minus'])/self.network.edges[a]['length']
                + self.parameters["gravity_coefficient"]*self.network.edges[a]['slope']*self.parameters["rho"]/1e5)
                self.network.edges[a]['bigM2'] = ((self.network.nodes[a[1]]['p_minus'] - self.network.nodes[a[0]]['p_plus'])/self.network.edges[a]['length']
                + self.parameters["gravity_coefficient"]*self.network.edges[a]['slope']*self.parameters["rho"]/1e5)
        print("Finished big-M/path computation")
                            
    def setup_computation(self):
        print("Starting model initialization")
        ####OPTIMIZATION PROBLEM####
        
        ###Conversion parameters and initialization###
        bar_to_Pa = 1e5
        C_to_K = 273
        Watt_to_kWatt = 1e3
        data_dict = {}
        hours_in_day = 24
        
        ###Non-slack data###

        ##Sets##
        #Edges#
        data_dict["Apc"] = {None:[a for a in self.network.edges if self.network.edges[a]['is_pipe']*(not self.network.edges[a]['existing'])]}
        data_dict["Ape"] = {None:[a for a in self.network.edges if self.network.edges[a]['is_pipe']*self.network.edges[a]['existing']]}
        data_dict["Acc"] = {None:[a for a in self.network.edges if self.network.edges[a]['is_consumer']*(not self.network.edges[a]['existing'])]}
        data_dict["Ace"] = {None:[a for a in self.network.edges if self.network.edges[a]['is_consumer']*self.network.edges[a]['existing']]}
        data_dict["Ad"] = {None:[a for a in self.network.edges if self.network.edges[a]['is_source']]}
        #Nodes#
        data_dict["Vc"] = {None:[n for n in self.network.nodes if (not self.network.nodes[n]['existing'])]}
        data_dict["Ve"] = {None:[n for n in self.network.nodes if  self.network.nodes[n]['existing']]}
        data_dict["Vff"] = {None:[n for n in self.network.nodes if self.network.nodes[n]['in_foreflow']]}
        data_dict["Vbf"] = {None:[n for n in self.network.nodes if self.network.nodes[n]['in_backflow']]}
        #Orientation related#
        data_dict["X"] = {None: ['0','La']}
        data_dict["delta_out"] = {None:[(u,a) for u in self.network.nodes for a in self.network.out_edges(u)]}
        data_dict["delta_in"] = {None:[(u,a) for u in self.network.nodes for a in self.network.in_edges(u)]}
        data_dict["pos_vel"] = {None:[(a) for a in self.network.edges if self.network.edges[a]["flow_is_fixed_pos"]]}
        data_dict["neg_vel"] = {None:[(a) for a in self.network.edges if self.network.edges[a]["flow_is_fixed_neg"]]}
        data_dict["O"] = {None:[(u,a) for u in self.network.nodes for a in self.network.out_edges(u) if self.network.edges[a]["flow_is_fixed_pos"]]
        + [(u,a) for u in self.network.nodes for a in self.network.in_edges(u) if self.network.edges[a]["flow_is_fixed_neg"]]}
        data_dict["I"] = {None:[(u,a) for u in self.network.nodes for a in self.network.out_edges(u) if self.network.edges[a]["flow_is_fixed_neg"]]
        + [(u,a) for u in self.network.nodes for a in self.network.in_edges(u) if self.network.edges[a]["flow_is_fixed_pos"]]}
        #Path related#
        data_dict["Pa"] = {None:[(a,b) for a in self.network.edges if (not self.network.edges[a]['existing']) for b in self.network.edges[a]['path']]}
        #Approx related
        d = self.computational_parameters["degree_of_approx"]
        data_dict["KLN"] = {None: [(k,l,n) for k in range(1,d+1) for l in range(d+1) for n in range(d+1) if l+k+n <= d]}
        
        ##Parameters##
        
        data_dict["upper_bound_on_Pw"] = {None: self.parameters["upper_bound_on_Pw"] == 1}
        data_dict["upper_bound_on_Pg"] = {None: self.parameters["upper_bound_on_Pg"] == 1}
        data_dict["upper_bound_on_Pp"] = {None: self.parameters["upper_bound_on_Pp"] == 1}
        data_dict["L"] = {a: self.network.edges[a]["length"] for a in self.network.edges if self.network.edges[a]["is_pipe"]}
        data_dict["D"] = {a: self.network.edges[a]["diameter"] for a in self.network.edges if self.network.edges[a]["is_pipe"]}
        data_dict["Area"] = {a: math.pi*(self.network.edges[a]["diameter"]/2)*(self.network.edges[a]["diameter"]/2) for a in self.network.edges if self.network.edges[a]["is_pipe"]}
        data_dict["h"] = {a: self.network.edges[a]["slope"] for a in self.network.edges if self.network.edges[a]["is_pipe"]}
        data_dict["Pc"] = {a: abs(self.network.edges[a]["consumer_factor"]/hours_in_day) for a in self.network.edges if self.network.edges[a]["is_consumer"]}
        data_dict["U"] = {a: self.network.edges[a]["ht_coefficient"] for a in self.network.edges if self.network.edges[a]["is_pipe"]}
        data_dict["alpha"] = {(a[0],a[1],k,l,n): self.network.edges[a]['alpha'][str((k,l,n))] for a in self.network.edges for k in range(1,d+1) for l in range(d+1) for n in range(d+1) if self.network.edges[a]["is_pipe"] if l+k+n <= d}
        data_dict["Tff"] = {a: self.network.edges[a]["temperature"]+15+C_to_K for a in self.network.edges if self.network.edges[a]["is_consumer"]}
        Tff_list = [self.network.edges[a]["temperature"]+15+C_to_K for a in self.network.edges if self.network.edges[a]["is_consumer"]]
        data_dict["Tbf"] = {a: self.network.edges[a]["temperature"]+C_to_K for a in self.network.edges if self.network.edges[a]["is_consumer"]}
        Tbf_list = [self.network.edges[a]["temperature"]+C_to_K for a in self.network.edges if self.network.edges[a]["is_consumer"]]
        def mean(l):
            return sum(l)/len(l)
        self.Tff_mean = mean(Tff_list)
        self.Tbf_mean = mean(Tbf_list)
        data_dict["Tsoil"] = {None: self.parameters["temp_ground"]+C_to_K}
        data_dict["cp"] = {None: self.parameters["specific_heat_coefficient"]}
        data_dict["ps"] = {None: self.parameters["stagnation_pressure"]}
        data_dict["g"] = {None: self.parameters["gravity_coefficient"]}
        def An_C(C,L,r):
            print(C)
            return C*r*pow(1+r,L)/(pow(1+r,L)-1)
        Cinv_pipe = {a: An_C(self.network.edges[a]["pipe_price"]*self.network.edges[a]["length"], self.parameters["life_pipe"], self.parameters["discount_rate"]) for a in self.network.edges if self.network.edges[a]["is_pipe"] and (not self.network.edges[a]['existing'])}
        Cinv_cons = {a: An_C(self.parameters["cost_investment_consumer"], self.parameters["life_consumer"], self.parameters["discount_rate"]) for a in self.network.edges if self.network.edges[a]["is_consumer"] and (not self.network.edges[a]['existing'])}
        Cinv_pipe.update(Cinv_cons)
        data_dict["Cinv"] = Cinv_pipe
        data_dict["w1"] = {None: self.parameters["price_for_consumers"]}
        data_dict["w2"] = {a: self.network.edges[a]["peak_cost"] for a in self.network.edges if self.network.edges[a]["is_consumer"] and (not self.network.edges[a]['existing'])}
        data_dict["Cw"] = {None: self.parameters["cost_waste_coef"]}
        data_dict["Cg"] = {None: self.parameters["cost_gas_coef"]}
        data_dict["Cp"] = {None: self.parameters["cost_pressure_coef"]}
        data_dict["lamb"] = {a:math.pow(2*math.log10(self.network.edges[a]["diameter"]/self.network.edges[a]["roughness"],10)+1.138,-2) for a in self.network.edges if self.network.edges[a]["is_pipe"]}
        data_dict["q_plus"] = {a: self.network.edges[a]['q_plus'] for a in self.network.edges}
        data_dict["Temp_edge_plus"] = {a: self.network.edges[a]['Temp_plus']+C_to_K for a in self.network.edges}
        data_dict["Temp_edge_minus"] = {a: self.network.edges[a]['Temp_minus']+C_to_K for a in self.network.edges if (not self.network.edges[a]['is_consumer'])}
        data_dict["p_plus"] = {u: self.network.nodes[u]['p_plus'] for u in self.network.nodes}
        data_dict["p_minus"] = {u: self.network.nodes[u]['p_minus'] for u in self.network.nodes}
        data_dict["Temp_plus"] = {u: self.network.nodes[u]['Temp_plus']+C_to_K for u in self.network.nodes}
        data_dict["Temp_minus"] = {u: self.network.nodes[u]['Temp_minus']+C_to_K for u in self.network.nodes}
        data_dict["Pw_plus"] = {None: self.parameters["power_w_plus"]}
        data_dict["Pg_plus"] = {None: self.parameters["power_g_plus"]}
        data_dict["Pp_plus"] = {None: self.parameters["power_p_plus"]}
        data_dict["rho"] = {None: self.parameters["rho"]}
        data_dict["seconds_in_day"] = {None: self.parameters["seconds_in_day"]}
        data_dict["hours_in_day"] = {None: self.parameters["hours_in_day"]}
        data_dict["bigM1"] = {a: self.network.edges[a]['bigM1'] for a in self.network.edges if self.network.edges[a]['is_pipe'] and (not self.network.edges[a]['existing'])}
        data_dict["bigM2"] = {a: self.network.edges[a]['bigM2'] for a in self.network.edges if self.network.edges[a]['is_pipe'] and (not self.network.edges[a]['existing'])}

        
        ###Creation of instance###
        initial_state_computation_data = {None: data_dict}
        self.instance = self.m.create_instance(initial_state_computation_data)  
        self.instance.pprint()
        print("Finished model initialization")

    def solve_instance(self):
        
        ###WarmStart values of all parameters###
        self.instance.Pw.value = self.instance.Pw_plus.value
        self.instance.Pg.value = 1000
        self.instance.Pp.value = 1
        for u in self.instance.V:
            self.instance.p_node[u].value = self.instance.ps.value
        for a in self.instance.A:
            self.instance.q[a].value = 1
            if self.network.edges[a]['is_consumer']:
                self.instance.Temp_edge[a,'0'].value = self.instance.Tff[a]
                self.instance.Temp_edge[a,'La'].value = self.instance.Tbf[a]
                self.instance.Temp_node[a[0]].value = self.instance.Tff[a]
                self.instance.Temp_node[a[1]].value = self.instance.Tbf[a]
            elif self.network.edges[a]['is_pipe'] or self.network.edges[a]['is_source']:
                if self.network.nodes[a[0]]["in_foreflow"]:
                    if self.instance.Temp_node[a[0]].value == None:
                        self.instance.Temp_node[a[0]].value = self.Tff_mean
                    if self.instance.Temp_node[a[1]].value == None:
                        self.instance.Temp_node[a[1]].value = self.Tff_mean
                elif self.network.nodes[a[0]]["in_backflow"]:
                    if self.instance.Temp_node[a[0]].value == None:
                        self.instance.Temp_node[a[0]].value = self.Tbf_mean
                    if self.instance.Temp_node[a[1]].value == None:
                        self.instance.Temp_node[a[1]].value = self.Tbf_mean
                self.instance.Temp_edge[a,'0'].value = self.instance.Temp_node[a[0]].value
                self.instance.Temp_edge[a,'La'].value = self.instance.Temp_node[a[1]].value
        for a in self.instance.Ac:
            self.instance.x[a].value = 1
        ###Solve Model###
        self.opt = SolverFactory('gams')
        results = self.opt.solve(self.instance, solver=self.computational_parameters["solver"], tee=True, add_options=['option reslim=12000;', 'option optcr=0.001;'], warmstart=True, keepfiles=True)
        print("Solve Times")
        print(results.Solver[0]['User time'])
        print("Results")
        print('Connected?')
        for a in self.instance.Ac:
            print(a)
            print(self.instance.x[a].value)
        print('Pw')
        print(self.instance.Pw.value)
        print('Pp')
        print(self.instance.Pp.value)
        print('Pg')
        print(self.instance.Pg.value)
        print("Sum of water heating")
        print(self.instance.Pw.value + self.instance.Pg.value)
        print("Sum of demand")
        print(sum(value(self.instance.Pc[a])*value(self.instance.x[a]) for a in self.instance.Acc) + sum(value(self.instance.Pc[a]) for a in self.instance.Ace))

    @staticmethod
    def json_import(file_name):
        with open(file_name) as json_file:
            data = json.load(json_file)
        return data

    @staticmethod
    def table_to_csv(table, file_name, nl, dl):
        with open(file_name, 'w', newline=nl) as csvfile:
            csvwriter = csv.writer(csvfile, delimiter=dl, quoting=csv.QUOTE_NONE)
            for l in table:
                csvwriter.writerow(l)
    
    def save_results(self, file_name_nodes, file_name_edges, final_output):
        node_presets = self.json_import("/home/marius/Documents/Trier/DHN/dhn-expansion/code/src/data_extraction/node_presets.json")
        edge_presets = self.json_import("/home/marius/Documents/Trier/DHN/dhn-expansion/code/src/data_extraction/edge_presets.json")
        node_data = [node_presets[n].split('/') + [str(round(self.instance.p_node[n].value,2)) , str(round(self.instance.Temp_node[n].value,2))+","] for n in self.network.nodes if self.network.nodes[n]["in_foreflow"]]
        node_data[len(node_data)-1][len(node_data[len(node_data)-1])-1] = node_data[len(node_data)-1][len(node_data[len(node_data)-1])-1][0:-1]+'}'
        edge_data1 = [edge_presets[str(e)].split('/') + [str(round(self.instance.q[e].value,2)), "solid"*(self.instance.x[e].value == 1) + "dashed"*(self.instance.x[e].value == 0) + ","] for e in self.network.edges if (not self.network.edges[e]["existing"])  if self.network.nodes[e[0]]["in_foreflow"]  if self.network.nodes[e[1]]["in_foreflow"]]
        edge_data2 = [edge_presets[str(e)].split('/') + [str(round(self.instance.q[e].value,2)), "solid"+ ","] for e in self.network.edges if self.network.edges[e]["existing"] if self.network.nodes[e[0]]["in_foreflow"]  if self.network.nodes[e[1]]["in_foreflow"]]
        edge_data2[len(edge_data2)-1][len(edge_data2[len(edge_data2)-1])-1] = edge_data2[len(edge_data2)-1][len(edge_data2[len(edge_data2)-1])-1][0:-1]+'}'
        edge_data = edge_data1 + edge_data2
        self.table_to_csv(node_data, file_name_nodes, '\n', '/')
        self.table_to_csv(edge_data, file_name_edges, '\n', '/')
        filenames = ["/home/marius/Documents/Trier/DHN/dhn-expansion/code/src/data_extraction/header.tikz", "/home/marius/Documents/Trier/DHN/dhn-expansion/code/src/data_extraction/node_sol.tikz", "/home/marius/Documents/Trier/DHN/dhn-expansion/code/src/data_extraction/interm.tikz","/home/marius/Documents/Trier/DHN/dhn-expansion/code/src/data_extraction/edge_sol.tikz","/home/marius/Documents/Trier/DHN/dhn-expansion/code/src/data_extraction/closer.tikz"]
        with open(final_output, 'w') as outfile:
            for fname in filenames:
                with open(fname) as infile:
                    for line in infile:
                        outfile.write(line)
        
    def plot_results(self,with_colors = True):
        node_size,no_height_given,max_diff,factor,text_size,ax,fig = self.network.plot_network()
        offset = factor*max_diff/20
        q_values = [abs(self.instance.q[a].value) for a in self.instance.A]
        max_q_value = max(q_values)
        min_q_value = min(q_values)
        max_width = 4
        min_width = 1
        percentage = 0.25
        length_offset = 10
        n_after_comma = 6
        try:
            a = (max_width-min_width)/(max_q_value-min_q_value)
        except ZeroDivisionError:
            a = (max_width-min_width)/(max_q_value-min_q_value+1)
        b = max_width-a*max_q_value
        xborders = []
        yborders = []
        zborders = []
        def mean(l):
            return sum(l)/len(l)
        for n in self.instance.V:
            x = self.network.nodes[n]["position"][0]
            y = self.network.nodes[n]["position"][1]
            z = self.network.nodes[n]["position"][2]-max_diff*factor*no_height_given*self.network.nodes[n]["in_backflow"]
            ax.text(x,y,z - offset*self.network.nodes[n]["in_backflow"] + offset*self.network.nodes[n]["in_foreflow"],'p = '+str(round(self.instance.p_node[n].value,n_after_comma))+'\n'+'T = '+str(round(self.instance.Temp_node[n].value,n_after_comma)), ha='center', va= 'top'*self.network.nodes[n]["in_backflow"]+'bottom'*self.network.nodes[n]["in_foreflow"], size = text_size)
        for n1,n2 in self.instance.A:
            #Draw lines with changing width
            x_for_lines = [self.network.nodes[n1]["position"][0],self.network.nodes[n2]["position"][0]]
            y_for_lines = [self.network.nodes[n1]["position"][1],self.network.nodes[n2]["position"][1]]
            z_for_lines = [self.network.nodes[n1]["position"][2]-max_diff*factor*no_height_given*self.network.nodes[n1]["in_backflow"],self.network.nodes[n2]["position"][2]-max_diff*factor*no_height_given*self.network.nodes[n2]["in_backflow"]]
            width_of_line = a*abs(self.instance.q[(n1,n2)].value) + b
            ax.plot(x_for_lines,y_for_lines,z_for_lines,color=('red'*self.network.edges[(n1,n2)]['is_source']+'green'*self.network.edges[(n1,n2)]['is_consumer']+'blue'*self.network.edges[(n1,n2)]['is_pipe'])*with_colors+'blue'*(not with_colors), linewidth=width_of_line)
            ax.text(mean(x_for_lines) - 400*offset*(self.network.edges[(n1,n2)]["is_consumer"] or self.network.edges[(n1,n2)]["is_source"])  ,mean(y_for_lines) ,(mean(z_for_lines) - offset*self.network.edges[(n1,n2)]["is_pipe"]*self.network.nodes[n1]["in_foreflow"]+offset*self.network.edges[(n1,n2)]["is_pipe"]*self.network.nodes[n1]["in_backflow"]),'q = '+str(round(self.instance.q[(n1,n2)].value, n_after_comma)),ha='center', va= 'bottom'*self.network.nodes[n1]["in_backflow"]*self.network.edges[(n1,n2)]["is_pipe"]+'top'*self.network.nodes[n1]["in_foreflow"]*self.network.edges[(n1,n2)]["is_pipe"]+'center'*(self.network.edges[(n1,n2)]["is_consumer"] or self.network.edges[(n1,n2)]["is_source"]), size = text_size)
           
            x0 = self.network.nodes[n1]["position"][0]*(1-percentage) + self.network.nodes[n2]["position"][0]*percentage
            xLa = self.network.nodes[n1]["position"][0]*(percentage) + self.network.nodes[n2]["position"][0]*(1-percentage)
            y0 = self.network.nodes[n1]["position"][1]*(1-percentage) + self.network.nodes[n2]["position"][1]*percentage
            yLa = self.network.nodes[n1]["position"][1]*(percentage) + self.network.nodes[n2]["position"][1]*(1-percentage)
            z0 = (self.network.nodes[n1]["position"][2]-max_diff*factor*no_height_given*self.network.nodes[n1]["in_backflow"])*(1-percentage) + (self.network.nodes[n2]["position"][2]-max_diff*factor*no_height_given*self.network.nodes[n2]["in_backflow"])*percentage
            zLa = (self.network.nodes[n1]["position"][2]-max_diff*factor*no_height_given*self.network.nodes[n1]["in_backflow"])*(percentage) + (self.network.nodes[n2]["position"][2]-max_diff*factor*no_height_given*self.network.nodes[n2]["in_backflow"])*(1-percentage)
            xborders.append([x0,xLa])
            yborders.append([y0,yLa])
            zborders.append([z0,zLa])
            ax.text(x0 + 400*offset*(self.network.edges[(n1,n2)]["is_consumer"] or self.network.edges[(n1,n2)]["is_source"]), y0 , z0 + offset*self.network.edges[(n1,n2)]["is_pipe"]*self.network.nodes[n1]["in_foreflow"] - offset*self.network.edges[(n1,n2)]["is_pipe"]*self.network.nodes[n1]["in_backflow"], 'T = '+str(round(self.instance.Temp_edge[(n1,n2),'0'].value,n_after_comma)), ha='center', va= 'bottom'*self.network.nodes[n1]["in_backflow"]*self.network.edges[(n1,n2)]["is_pipe"]+'top'*self.network.nodes[n1]["in_foreflow"]*self.network.edges[(n1,n2)]["is_pipe"]+'center'*(self.network.edges[(n1,n2)]["is_consumer"] or self.network.edges[(n1,n2)]["is_source"]), size = text_size)
            ax.text(xLa + 400*offset*(self.network.edges[(n1,n2)]["is_consumer"] or self.network.edges[(n1,n2)]["is_source"]), yLa , zLa + offset*self.network.edges[(n1,n2)]["is_pipe"]*self.network.nodes[n1]["in_foreflow"] - offset*self.network.edges[(n1,n2)]["is_pipe"]*self.network.nodes[n1]["in_backflow"], 'T = '+str(round(self.instance.Temp_edge[(n1,n2),'La'].value,n_after_comma)), ha='center', va= 'bottom'*self.network.nodes[n1]["in_backflow"]*self.network.edges[(n1,n2)]["is_pipe"]+'top'*self.network.nodes[n1]["in_foreflow"]*self.network.edges[(n1,n2)]["is_pipe"]+'center'*(self.network.edges[(n1,n2)]["is_consumer"] or self.network.edges[(n1,n2)]["is_source"]), size = text_size)
        ax.scatter(xborders,yborders,zborders, s=node_size/4, color='black', alpha = 1)
        fig.show()
        input()
